<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SimplePost extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SimplePost:sendsomething';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new \GuzzleHttp\Client();
        
        $url="https://atomic.incfile.com/fakepost";
        $params = ['form_params'=> array('email'=> 'test@email.com','user'=>'Test User')];
        $response = $client->post( $url , $params);
        dd($response);
        
    }
}
